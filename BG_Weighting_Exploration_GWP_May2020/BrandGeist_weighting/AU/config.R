list(
	main = list(
		WEIGHT_MAIN_ISC = list(
			"conditions" = list(
				"data" = list(bd_country = 12),  #change for each country
				"target" = list()
			),
			"preweight" = "Weight",
			"balance" = FALSE
		)
	),
	deps = list(
	),
	default = list(
		"trim" = 2,
		"nonmember" = 0,
		"incomplete" = 0,
		"mintarget" = 100,
		"mindata" = 100,
		"minviolate" = 1,
		"nonconverge" = 1,
		"bounds" = c(0.1, 3),
		"hardbind" = FALSE,
		"controls" = c(
			"AGEGEN_FOR_WGT",
			"EDU_FOR_WGT",
			"REG_FOR_WGT"  # add comma for US only
#			"RCE_FOR_WGT"  #change for US only
		)
	),
	files = list(
		"config" = "config.R",
		"data" = "data.csv",
		"target" = "../target_CCS/AU19FEB_ccs.csv",  #change for each country
		"log" = "log.txt",
		"benchmarks" = "benchmarks.csv",
		"assessments" = "assessments.csv",
		"weights" = "weighted_isc.csv"
	)
)