﻿* Encoding: UTF-8.

GET FILE 'I:\SAS_MS\19-091677-01\BrandPulse weighting\FEB 2020\BR20FEB\Feb20_BR_IntUse.sav'.
 DATASET NAME DataSet1 WINDOW=FRONT.



**PNA or prefer not to answer will not appear in the ccs file, however, there are some attributes that allow for PNA.  As new responses are added - mainly when new countries are fielded -
this PNA code may change.  It will NOT change in a single wave.  You will want to confirm the PNA code when starting a new wave**

**variables used for "special" KTAs created from combinations of existing KTAs**

IF (H_ktaqual_1=1 OR H_ktaqual_11=1) H_ktaqual_spec_18to44=1.
IF (H_KTAassign_01=1 OR H_KTAassign_11=1) H_KTAassign_spec_18to44=1.
IF (H_prodassign_2=1 OR H_prodassign_23=1) H_prodassign_spec_18to44=1.
EXECUTE.

RECODE
H_ktaqual_spec_18to44
H_KTAassign_spec_18to44
H_prodassign_spec_18to44
(1=1) (ELSE=0).
EXECUTE.


*using S0101_age and S0102_gender - create the AGEGEN_FOR_WGT column to include the age break and genders identified in the ccs target file.  MUST be string values 
EXACTLY MATCHING the format in the ccs target file**


STRING AGEGEN_FOR_WGT (a10).

IF (ANY (S0101_age,16,17,18,19,20,21,22,23,24) AND (S0102_gender=1)) AGEGEN_FOR_WGT='[16,25)M'.
IF (ANY (S0101_age,25,26,27,28,29,30,31,32,33,34) AND (S0102_gender=1)) AGEGEN_FOR_WGT='[25,35)M'.
IF (ANY (S0101_age,35,36,37,38,39,40,41,42,43,44) AND (S0102_gender=1)) AGEGEN_FOR_WGT='[35,45)M'.
IF (ANY (S0101_age,45,46,47,48,49,50,51,52,53,54) AND (S0102_gender=1)) AGEGEN_FOR_WGT='[45,55)M'.
IF (ANY (S0101_age,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70) AND (S0102_gender=1)) AGEGEN_FOR_WGT='[55,Inf]M'.
IF (ANY (S0101_age,16,17,18,19,20,21,22,23,24) AND (S0102_gender=2)) AGEGEN_FOR_WGT='[16,25)F'.
IF (ANY (S0101_age,25,26,27,28,29,30,31,32,33,34) AND (S0102_gender=2)) AGEGEN_FOR_WGT='[25,35)F'.
IF (ANY (S0101_age,35,36,37,38,39,40,41,42,43,44) AND (S0102_gender=2)) AGEGEN_FOR_WGT='[35,45)F'.
IF (ANY (S0101_age,45,46,47,48,49,50,51,52,53,54) AND (S0102_gender=2)) AGEGEN_FOR_WGT='[45,55)F'.
IF (ANY (S0101_age,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70) AND (S0102_gender=2)) AGEGEN_FOR_WGT='[55,Inf]F'.
EXECUTE.


**MERGE the H_prodassign_x into a single column - each respondent will only be assigned to one product line**

**Beginning June 2019 wave, the following products are being excluded at scripting - 6, 11, 12, 13, 15, 24-28**
**Beginning Sep 2019 wave, the following products are added at 32 and 33.
**Beginning Jan 2020 wave, product 34 is added.

STRING Assignment (a20).

IF H_prodassign_1=1 Assignment='H_prodassign_1'.
IF H_prodassign_2=1 Assignment='H_prodassign_2'.
IF H_prodassign_3=1 Assignment='H_prodassign_3'.
IF H_prodassign_4=1 Assignment='H_prodassign_4'.
IF H_prodassign_5=1 Assignment='H_prodassign_5'.
IF H_prodassign_7=1 Assignment='H_prodassign_7'.
IF H_prodassign_8=1 Assignment='H_prodassign_8'.
IF H_prodassign_9=1 Assignment='H_prodassign_9'.
IF H_prodassign_10=1 Assignment='H_prodassign_10'.
IF H_prodassign_14=1 Assignment='H_prodassign_14'.
IF H_prodassign_16=1 Assignment='H_prodassign_16'.
IF H_prodassign_17=1 Assignment='H_prodassign_17'.
IF H_prodassign_18=1 Assignment='H_prodassign_18'.
IF H_prodassign_19=1 Assignment='H_prodassign_19'.
IF H_prodassign_20=1 Assignment='H_prodassign_20'.
IF H_prodassign_21=1 Assignment='H_prodassign_21'.
IF H_prodassign_22=1 Assignment='H_prodassign_22'.
IF H_prodassign_23=1 Assignment='H_prodassign_23'.
IF H_prodassign_29=1 Assignment='H_prodassign_29'.
IF H_prodassign_30=1 Assignment='H_prodassign_30'.
IF H_prodassign_31=1 Assignment='H_prodassign_31'.
IF H_prodassign_32=1 Assignment='H_prodassign_32'.
IF H_prodassign_33=1 Assignment='H_prodassign_33'.
IF H_prodassign_34=1 Assignment='H_prodassign_34'.
EXECUTE.


**Groups Education responses into high, medium and low.  Maps.R can be referenced to identify which punches fall into each category.  NOTE: all possible education responses for 
all countries are included in value labels.  Be sure to identify the correct values for the country you are weighting.  High/Mid/Low groupings will change by country**

**Low - 1,2;  Mid - 3,4,5;  High - 6**

STRING EDUX_FOR_WGT (a5).

IF (ANY (QD0901_educ,8,9)) EDUX_FOR_WGT='Low'.
IF (ANY (QD0901_educ,10,11,12)) EDUX_FOR_WGT='Mid'.
IF (ANY (QD0901_educ,13)) EDUX_FOR_WGT='High'.
IF (QD0901_educ=225) EDUX_FOR_WGT='PNA'.
EXECUTE.



**recodes region value into a two character code.  In most countries this will be a 1-1 recode.  Maps.R can be referenced to identify which punches fall into each category.  **

** "N" = 12, Norte; "NE" = 13, Nordeste; "SE" = 14, Sudeste; "S" = 15, Sul; "ME" = 16, Centro-Oeste 

STRING REG_FOR_WGT (a4).

IF RegionQuotaMarker=12 REG_FOR_WGT='N'.
IF RegionQuotaMarker=13 REG_FOR_WGT='NE'.
IF RegionQuotaMarker=14 REG_FOR_WGT='SE'.
IF RegionQuotaMarker=15 REG_FOR_WGT='S'.
IF RegionQuotaMarker=16 REG_FOR_WGT='ME'.
EXECUTE.



SAVE TRANSLATE OUTFILE='I:\SAS_MS\19-091677-01\BrandPulse weighting\FEB 2020\BR20FEB\20FEB_data.csv'
  /TYPE=CSV
  /ENCODING='UTF8'
  /MAP
  /REPLACE
  /FIELDNAMES
  /CELLS=VALUES.




**The following frequencies/crosstabs should allow you to confirm the new variables have been coded correctly.**


CROSSTABS
  /TABLES=H_prodassign_1 H_prodassign_2 H_prodassign_3 H_prodassign_4 H_prodassign_5 
    H_prodassign_7 H_prodassign_8 H_prodassign_9 H_prodassign_10 H_prodassign_14 H_prodassign_16 
    H_prodassign_17 H_prodassign_18 H_prodassign_19 H_prodassign_20 H_prodassign_21 
    H_prodassign_22 H_prodassign_23 H_prodassign_29 H_prodassign_30 H_prodassign_31 
    H_prodassign_32 H_prodassign_33 H_prodassign_34 BY Assignment
  /FORMAT=AVALUE TABLES
  /CELLS=COUNT
  /COUNT ROUND CELL.


CROSSTABS
  /TABLES=S0101_AgeBreaks_2 BY S0102_gender BY AGEGEN_FOR_WGT
  /FORMAT=AVALUE TABLES
  /CELLS=COUNT
  /COUNT ROUND CELL.


CROSSTABS
  /TABLES=EDUX_FOR_WGT BY QD0901_educ
  /FORMAT=AVALUE TABLES
  /CELLS=COUNT
  /COUNT ROUND CELL.


CROSSTABS
  /TABLES=REG_FOR_WGT BY RegionQuotaMarker
  /FORMAT=AVALUE TABLES
  /CELLS=COUNT
  /COUNT ROUND CELL.


FREQUENCIES VARIABLES=AGEGEN_FOR_WGT Assignment EDUX_FOR_WGT REG_FOR_WGT 
  /ORDER=ANALYSIS.