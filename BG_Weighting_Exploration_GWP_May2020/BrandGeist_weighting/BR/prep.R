(function() {
    d <- as.data.frame(
        haven::read_spss("../../SPSS_W6/GoogleBrandgeist_Allwaves_BR_V1_IntUse_VariablePlan.sav")   
    )

    d <- d[which(d$fv_wave==6), ]

    names(d)[names(d) == "weight"] <- "WEIGHT_MAIN_DP"

    d$AGE_FOR_WGT <- as.character(
        cut(
            d$bd_age,
            c(16, 25, 35, 45, 55, 70),  # 70 used to be INF
            include.lowest = TRUE,
            right = FALSE
        )
    )


    d$GEN_FOR_WGT <- c("M", "F")[d$bd_gender]


    d$AGEGEN_FOR_WGT <- (function() {
        age_gen_combos <- expand.grid(
            unique(d$AGE_FOR_WGT),
            unique(d$GEN_FOR_WGT),
            stringsAsFactors = FALSE
        )
        age_gen_x <- apply(age_gen_combos, 1, function(x) paste0(x, collapse = ""))
        age_gen_combos$val <- age_gen_x
        names(age_gen_combos) <- c(
            "AGE_FOR_WGT",
            "GEN_FOR_WGT",
            "AGEGEN_FOR_WGT"
        )
        age_gen_combos <- age_gen_combos[
            !is.na(age_gen_combos$AGE_FOR_WGT) &
            !is.na(age_gen_combos$GEN_FOR_WGT),
        ]
        data <- dplyr::left_join(
            d,
            age_gen_combos,
            by = c("AGE_FOR_WGT", "GEN_FOR_WGT")
        )
        return(data$AGEGEN_FOR_WGT)
    })()


    d$EDU_FOR_WGT <- c(
        "Low",
        "Low",
        "Mid",
        "Mid",
        "Mid",
        "High",
        "PNA"
    )[match(
        d$bd_education,
        c(1301, 1302, 1303, 1304, 1305, 1306, 9999)
    )]
    

    d$REG_FOR_WGT <- c("N", "NE", "SE", "S", "ME")[
        match(
            d$bd_region,
            c(1301, 1302, 1303, 1304, 1305)
        )
    ]

    write.csv(d, file = "data.csv", row.names = FALSE)
})()
